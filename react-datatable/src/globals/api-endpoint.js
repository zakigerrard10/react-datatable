import CONFIG from "./config";

const API_ENDPOINT = {
  Comments: {
    getAll: `${CONFIG.Base_URL}/comments`,
    delete: (id) => `${CONFIG.Base_URL}/comments/${id}`,
  },
};

export default API_ENDPOINT;