import axios from "axios";
import API_ENDPOINT from "../globals/api-endpoint";

const CommentIdb = {
    getComment : async() =>{
        return await axios.get(API_ENDPOINT.Comments.getAll); 
    },
    deleteComment : async (id) => {
        return await axios.delete(`${API_ENDPOINT.Comments.delete(id)}`)
    }
}

export default CommentIdb;