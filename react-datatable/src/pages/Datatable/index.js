import { Button, Popconfirm, Table, message, Space, Input, Form } from 'antd';
import React, { useEffect, useState } from 'react'
import CommentIdb from '../../data/CommentsIdb';

const Datatable = () => {

  const [comments, setComments] = useState([]);
  const [fetchStatus, setFetchStatus] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (fetchStatus === false) {
      setLoading(true)
      CommentIdb.getComment()
        .then((res) => {
          const data = res.data;
          setComments(
            data.map((comment, index) => {
              return {
                key: comment.id,
                no: index + 1,
                id: comment.id,
                name: comment.name,
                email: comment.email,
                body: comment.body,
              };
            }),
          );
          setFetchStatus(true)
          setLoading(false);
        })
        .catch((err) => console.error(`masalah ${err.message}`));
    }
  },[fetchStatus]);

  const handleDelete = (idComment) => {
    CommentIdb.deleteComment(idComment).then((res) => {
      setLoading(true)
      const newDataComment = comments.filter((comment) => comment.id !== idComment);
      setComments(newDataComment);
      message.success('Data Berhasil Terhapus');
      setLoading(false);
    }).catch((err) => console.error(`Masalah : ${err.message}`));
  }

  const dataSource = comments;
  const columns = [
    {
      title: 'No',
      dataIndex: 'no',
      key: 'no',
      sorter: (a, b) => a.no - b.no,
      width: '5%',
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      align: 'center',
      sorter: (a, b) => {
        if (a.name < b.name) {
          return -1;
        }
        if (a.name > b.name) {
          return 1;
        }
        return 0;
      },
      width: '25%',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
      align: 'center',
      sorter: (a, b) => {
        if (a.email < b.email) {
          return -1;
        }
        if (a.email > b.email) {
          return 1;
        }
        return 0;
      },
      width: '25%',
    },
    {
      title: 'Comment',
      dataIndex: 'body',
      key: 'body',
      align: 'center',
      width: '45%',
    },
    {
      title: 'Action',
      dataIndex: 'action',
      align: 'center',
      render: (index, comment) => {
        return (
          <React.Fragment>
            <Space>
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <Popconfirm
                  placement='bottomRight'
                  onConfirm={() => handleDelete(comment.id)}
                  title={'Apakah Data Ini Ingin Dihapus'}
                  onCancel={() => message.error('Data Tidak Terhapus')}
                >
                  <Button
                    type='primary'
                    danger
                    style={{ borderRadius: '4px' }}
                  >
                    Delete
                  </Button>
                </Popconfirm>
              </div>
            </Space>
          </React.Fragment>
        );
      },
    },
  ];

  return (
    <React.Fragment>
      <Table
        dataSource={dataSource}
        columns={columns}
        loading={loading}
        size='middle'
      />
    </React.Fragment>
  );
}

export default Datatable
