import './App.css';
import React from 'react';
import Datatable from './pages/Datatable';

const App = () => {
  return (
    <React.Fragment>
      <Datatable />
    </React.Fragment>
  )
}

export default App;
